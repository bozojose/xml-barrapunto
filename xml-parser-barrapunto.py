#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from xml.sax.handler import ContentHandler
from xml.sax import make_parser
import sys
import urllib.request

class CounterHandler(ContentHandler):

    def __init__(self):
        self.inItem= False
        self.inContent = False
        self.theContent = ""
        self.titulo = ""
        self.enlace = ""

    def startElement(self, name, attrs):  # Se ejecuta cada vez que empieza una etiqueta
        if name == 'item':
            self.inItem = True
        elif self.inItem:
            if name == 'title':  # indica que lo que va a venir es de interes
                self.inContent = True
            elif name == 'link':  # indica que lo que va a venir es de interes
                self.inContent = True

    def endElement(self, name):  # se ejecuta cuando reconoce un elemento terminal
        if name == 'item':
            self.inItem = False
            print("<li><a href='" + self.enlace + "'>" + self.titulo + "</a></li>")
        elif self.inItem:
            if name == 'title':
                self.titulo = self.theContent
                self.inContent = False
                self.theContent = ""
            elif name == 'link':
                self.enlace = self.theContent
                self.inContent = False
                self.theContent = ""

    def characters(self, chars):
        if self.inContent:
            self.theContent = self.theContent + chars

# --- Main prog

if len(sys.argv) < 1:
    print ("Usage: python3 xml-parser-barrapunto.py")
    print()
    print("Usage: python3 xml-parser-barrapunto.py > <document>")
    print()
    print("<document>: nombre del fichero destino (.html)")
    sys.exit(1)

# Load parser and driver

BarrapuntoParser = make_parser()  # crea un parser sax estandar
BarrapuntoHandler = CounterHandler()  # instancia un objeto de la clase que se creo arriba
BarrapuntoParser.setContentHandler(BarrapuntoHandler)  # le dice al parser sax que utilice el objeto instanciado (con esto ya tenemos el manejador con el parser sax)

# Ready, set, go!
print("<html><head><h2>Titulares de BarraPunto</h2><body><ul>") #para que salga el contenido html correctamente
xmlFile = urllib.request.urlopen('http://barrapunto.com/index.rss')  # abre el doc
BarrapuntoParser.parse(xmlFile) # parsea el documento
print("</ul></body></head></html>") #para que salga el contenido html correctamente